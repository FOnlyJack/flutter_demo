import 'package:flutter/material.dart';

class GlobalConfig {
  static bool dark = false;
  static ThemeData themeData = ThemeData(
    primaryColor: Color(0xff2196f3),
    scaffoldBackgroundColor: Color(0xFFEBEBEB),
  );
  static Color searchBackgroundColor = Color(0xFFEBEBEB);
  static Color cardBackgroundColor = Colors.white;
  static Color fontColor = Colors.black54;
}
